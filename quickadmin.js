(function ($) {
  Drupal.behaviors.quickadmin = {
    attach: function(context) {
      // Hide the go button by default, the whole point of this module is to do
      // everything with the keyboard. Use Enter to submit the form instead.
      $('#quickadmin-menu-form .form-actions').addClass('element-invisible');

      // Provide an HTML5 placeholder
      $('#edit-quickadmin-search').attr('placeholder', 'Search admin menu for...');

      // Show the search box on ctrl+m
      Mousetrap.bind('ctrl+m', function(e) {
        $('#show-quickadmin').toggleClass('element-invisible');
        $('#edit-quickadmin-search').focus();
      });

      // Hide the form when the textfield loses focus
      $('#edit-quickadmin-search').blur(function(){
        $('#show-quickadmin').addClass('element-invisible');
      });

      // Remove focus when pressing escape
      Mousetrap.bindGlobal('esc', function() {
        $('#edit-quickadmin-search').blur();
      });
    }
  };
})(jQuery);
