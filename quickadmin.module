<?php

/**
 * @file
 * Displays and handles the admin search form.
 */

/**
 * Implements hook_permission().
 */
function quickadmin_permission() {
  return array(
    'use quickadmin' => array(
      'title' => t('Use quickadmin search'),
      'description' => t('Use quickadmin to find and navigate to admin pages.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function quickadmin_menu() {
  $items['quickadmin/autocomplete'] = array(
    'page callback' => 'quickadmin_autocomplete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Provides the form with autocomplete field.
 */
function quickadmin_menu_form() {
  $form['quickadmin_search'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'quickadmin/autocomplete',
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );
  $form['#submit'][] = 'quickadmin_go';
  return $form;
}

/**
 * Handles the actual autocomplete with a db query.
 */
function quickadmin_autocomplete($string) {
  $matches = array();

  // Select rows that match the string.
  $query = db_select('menu_links', 'ml')
    ->fields('ml', array('link_title', 'link_path'))
    ->condition('ml.link_path', 'admin%', 'LIKE')
    ->condition('ml.link_path', '%/\%%', 'NOT LIKE');
  $query->join('menu_router', 'mr', 'ml.link_path = mr.path');
  $query->fields('mr', array('description'));
  $or = db_or()
    ->condition('mr.description', '%' . db_like($string) . '%', 'LIKE')
    ->condition('mr.title', '%' . db_like($string) . '%', 'LIKE');
  $query->condition($or);

  $results = $query
    ->range(0, 10)
    ->execute();

  // Add matches to $matches.
  // Key is path, value is link title with description with matches bolded.
  foreach ($results as $result) {
    $matches[$result->link_path] = str_replace($string, '<strong>' . $string . '</strong>', $result->link_title . '&nbsp;&nbsp;&nbsp;<span style="color: #00E">/' . $result->link_path . '</span><br/><small>' . $result->description . '</small>');
  }

  // Return for JS.
  drupal_json_output($matches);
}

/**
 * Redirects on form submit - go to the specified page.
 */
function quickadmin_go($form, &$form_state) {
  $item = $form_state['values']['quickadmin_search'];
  if (isset($item)) {
    $result = db_select('menu_router', 'mr')
      ->fields('mr', array('path'))
      ->condition('mr.path', $item)
      ->execute()
      ->fetchAssoc();
    drupal_goto($result['path']);
  }
}

/**
 * Add the form to all pages whenever the user has permission.
 */
function quickadmin_page_alter(&$page) {
  if (user_access('use quickadmin')) {
    $library = libraries_load('mousetrap');
    if ($library['loaded'] !== FALSE) {
      drupal_add_js(drupal_get_path('module', 'quickadmin') . '/quickadmin.js');
      $form = drupal_get_form('quickadmin_menu_form');
      $page['page_top']['quickadmin'] = array(
        '#markup' => drupal_render($form),
        '#prefix' => '<div id="show-quickadmin" class="element-invisible">',
        '#suffix' => '</div>',
      );
    }
    else {
      drupal_set_message(t('Failed to load library %library for QuickAdmin.', array('%library' => 'Mousetrap')), 'warning');
    }
  }
}

/**
 * Define the mousetrap library for use.
 */
function quickadmin_libraries_info() {
  $libraries['mousetrap'] = array(
    'name' => 'Mousetrap',
    'library path' => libraries_get_path('mousetrap'),
    'files' => array(
      'js' => array('mousetrap.min.js', 'plugins/global-bind/mousetrap-global-bind.min.js'),
    ),
    'version arguments' => array(
      'file' => 'mousetrap.js',
      'pattern' => '@version\s+([0-9a-zA-Z\.-]+)@',
    ),
  );
  return $libraries;
}
