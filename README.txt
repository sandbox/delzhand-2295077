This module requires the mousetrap library, which handles JS keybindings.  It
can be found at https://github.com/ccampbell/mousetrap.git and must be
installed in sites/all/libraries/mousetrap so that the following files exist:

sites/all/libraries/mousetrap/mousetrap.min.js
sites/all/libraries/mousetrap/plugins/global-bind/mousetrap-global-bind.min.js

Once enabled, any user with the appropriate permission can show the search box
at any time by pressing ctrl+m (Win/Linux) or cmd+m (OSX).  The search box is
automatically hidden if it loses focus or if you press escape.
